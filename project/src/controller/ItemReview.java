package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.ReviewDataBeans;
import dao.BuyDAO;
import dao.ItemDAO;
import dao.ReviewDAO;

/**
 * Servlet implementation class ItemReview
 */
@WebServlet("/ItemReview")
public class ItemReview extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemReview() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		try {

			String score = request.getParameter("score");
			String title = request.getParameter("title");
			String comment = request.getParameter("comment");
			int itemId = Integer.parseInt(request.getParameter("item_id"));
			int userId = (int) session.getAttribute("userId");


			ReviewDAO.createReview(score, title, comment, itemId,  userId);


			ItemDataBeans item = ItemDAO.getItemByItemID(itemId);
			request.setAttribute("item", item);




			int count = BuyDAO.buyInfo(userId, itemId);

			request.setAttribute("count", count);



			if (ReviewDAO.findAllById(itemId) != null) {
				List<ReviewDataBeans> rdbList = ReviewDAO.findAllById(itemId);

				request.setAttribute("rdbList", rdbList);
			}


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
			dispatcher.forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
		}










	}

}
