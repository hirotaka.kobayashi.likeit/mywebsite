<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">

		<h5 class="title text-center">検索結果</h5>

		 <div class="row">
				<c:forEach var="item" items="${itemList}">
						<div class="col-6">
							<div class="card" style="width: 20rem;">
								  <img src="img/${item.fileName}">
								  <div class="card-body">
								   	<p>${item.name}</p>
									<a href="Item?item_id=${item.id}" class="btn btn-primary">アイテム詳細へ</a>
								  </div>
					 		</div>
					 		<div class="section"></div>
					 	</div>

			 	</c:forEach>
		 	</div>


	</div>
</body>
</html>