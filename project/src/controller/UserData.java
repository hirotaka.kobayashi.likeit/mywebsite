package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			int userId = (int) session.getAttribute("userId");
			UserDataBeans udb = UserDAO.getUserDataBeansByUserId(userId);


			request.setAttribute("udb", udb);

			ArrayList<BuyDataBeans> bdbList = BuyDAO.getAllBuyDataBeans(userId);

			request.setAttribute("bdbList", bdbList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userData.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		try {

			request.setCharacterEncoding("UTF-8");
			HttpSession session = request.getSession();

			String id = request.getParameter("id");
			String loginId = request.getParameter("login_id");
			String  user_name = request.getParameter("user_name");
			String user_address = request.getParameter("user_address");

			if (loginId.equals("") || user_name.equals("") || user_address.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくはありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userData.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDataBeans user =  UserDAO.check(loginId);

			if (!(user == null) ) {
				request.setAttribute("errMsg", "ほかのユーザーが使用中のログインIDです");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userData.jsp");
				dispatcher.forward(request, response);
				return;
			}


			 UserDAO.updateUser(loginId, user_name, user_address, id);
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDataUpdate.jsp");
			 dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}



	}

}
