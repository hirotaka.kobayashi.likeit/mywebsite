package beans;

import java.io.Serializable;
import java.util.Date;

public class UserDataBeans implements Serializable {
	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private Date birthDate;
	private String createDate;

	public UserDataBeans(int id, String loginId, String name, String address, Date birthDate, String password, String createDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.address = address;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;

	}



	public UserDataBeans(int id , String name) {
		this.id = id;
		this.name = name;

	}

	public UserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public void setUpdateUserDataBeansInfo(String name, String loginId, String address, int id) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
	}
}
