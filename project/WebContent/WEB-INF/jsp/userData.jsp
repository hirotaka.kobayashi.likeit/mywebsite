<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h3>ユーザー情報</h3>
		<div class="section"></div>
		<form action="UserData" method="post">
		    <div class="block">
		    	<input type="hidden" name="id" value="${udb.id}">
				<div class="row">
					<div class="col-4">
						<label>ログインID:</label>
						<input type="text" class="userData"  name="login_id"  value="${udb.loginId}">
					</div>
					<div class="col-4">
						<label>ユーザー名:</label>
						<input type="text"  class="userData" name="user_name" value="${udb.name}">
					</div>
					<div class="col-4">
						<label>住所:</label>
						<input type="text" class="userData" name="user_address" value="${udb.address}">
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success" type="submit" name="action">更新</button>
				</div>
			</div>
		</form>

		<div class="section"></div>

		<div class="buydata">
			<table class="table">
				<thead>
					<tr>
						<th style="width:10%;"></th>
						<th>購入日時</th>
						<th>配達方法</th>
						<th>購入金額</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="bdb" items="${bdbList}">
					<tr>
					    <td><a href="UserBuyHistoryDetail?buy_id=${bdb.id}"><i class="material-icons">details</i></a></td>
						<td class="center">${bdb.formatDate}</td>
						<td class="center">${bdb.deliveryMethodName}</td>
						<td class="center">${bdb.formatTotalPrice}円</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
	</div>
</body>
</html>