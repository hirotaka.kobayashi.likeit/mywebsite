package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;



public class UserDAO {

	public static UserDataBeans check(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String addressData = rs.getString("address");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new UserDataBeans(idData, loginIdData, nameData, addressData ,birthDate, password, createDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static void regist(String loginId, String user_name, String user_address, String birth,String password) {

		Connection conn = null;


			conn = DBManager.getConnection();
			String insertSQL = "INSERT INTO user (login_id, name, address ,birth_date, password ,create_date) VALUES (?,?,?,?,?,now())";
			PreparedStatement pStmt;
			try {
				pStmt = conn.prepareStatement(insertSQL);
				pStmt.setString(1, loginId);
				pStmt.setString(2, user_name);
				pStmt.setString(3, user_address);
				pStmt.setString(4, birth);
				pStmt.setString(5, password);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}


	}

	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? AND password = ?");
			st.setString(1, loginId);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				userId = rs.getInt("id");
			}
			return userId;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id,name, login_id, address FROM user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setAddress(rs.getString("address"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}


	public static void updateUser(String loginId, String name, String address, String id) throws SQLException {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String updateuserSQL = "UPDATE user SET login_id = ?, name = ?, address = ?  WHERE id= ?";
			PreparedStatement pStmt = conn.prepareStatement(updateuserSQL);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, address);
			pStmt.setString(4, id);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}


	public static List<UserDataBeans> findAll() throws SQLException {
		Connection conn = null;
		List<UserDataBeans>  userList = new ArrayList<UserDataBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id != 1";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");

				UserDataBeans user = new UserDataBeans(id, loginId, name, address, birthDate, password, createDate);
				userList.add(user);

			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;

	}


	public static UserDataBeans find(String id) throws SQLException {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int iddata = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String addressData = rs.getString("address");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");

			return new UserDataBeans(iddata, loginIdData, nameData, addressData, birthDate, password, createDate);


		}  catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static List<UserDataBeans> search (String user_loginId,String user_name,String user_startDate,String user_endDate) throws SQLException {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			conn = DBManager.getConnection();

			String searchsql = "SELECT * FROM user WHERE id !=1 ";

			if(!user_loginId.equals("")) {
				searchsql += " AND login_id = '" + user_loginId + "'";
			}

			if(!user_name.equals("")) {
				searchsql += " AND name LIKE '%" + user_name + "%'";
			}

			if(!user_startDate.equals("")) {
				searchsql += " AND birth_date >= '" + user_startDate + "' ";
			}

			if(!user_endDate.equals("")) {
				searchsql += " AND birth_date <= '" + user_endDate + "' ";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(searchsql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				UserDataBeans user = new UserDataBeans(id, loginId, name, address, birthDate, password, createDate);

				userList.add(user);

			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;
	}


	public static void deleteUser(String id) throws SQLException {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String deleteSQL = "DELETE FROM user WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(deleteSQL);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
