<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1 class="text-center">新規登録</h1>

		<c:if test="${errMsg != null}" >
			    <div class="alert alert-danger" role="alert">
				  ${errMsg}
				</div>
			</c:if>

		<div class="regist">
			<form action="Regist" method="POST">
				<div class="form-group">
					<label>ログインID</label>
					<input type="text" name="login_id" class="form-control">
				</div>
				<div class="form-group">
					<label>ユーザー名</label>
					<input type="text" name="user_name"  class="form-control">
				</div>
				<div class="form-group">
					<label>住所</label>
					<input type="text" name="user_address" class="form-control">
				</div>
				<div class="form-group">
					<label>生年月日</label>
					<input type="date" name="birth" class="form-control">
				</div>
				<div class="form-group">
					<label>パスワード</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<label>パスワード確認</label>
					<input type="password" name="pwd_conf" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">登録</button>
			</form>
		</div>
	</div>
</body>
</html>