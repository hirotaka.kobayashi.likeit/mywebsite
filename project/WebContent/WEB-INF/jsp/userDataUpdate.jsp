<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1 class="text-center">更新完了</h1>
		<div class="section"></div>
	    <div class="block">
	    	<div class="container">
				<form action="UserData">
					<div class="button_wrapper">
						<button class="btn btn-success">ユーザー情報へ</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>