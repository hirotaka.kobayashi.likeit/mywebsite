<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">

			<h1 class="login" >ログイン画面</h1>

			<form action="Login" method="post">
			  <div class="form-group row">
			    <label for="login" class="col-sm-4 col-form-label">ログインID</label>
			    <div class="col-md-8">
			      <input type="text"  name="loginId">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="password" class="col-sm-4 col-form-label">パスワード</label>
			    <div class="col-md-8">
			      <input type="password"  name="password">
			    </div>
			  </div>
			  <input type="submit" value="ログイン" class="button">
			</form>
	</div>
</body>
</html>