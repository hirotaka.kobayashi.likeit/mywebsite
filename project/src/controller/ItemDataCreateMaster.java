package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.ItemDAO;



/**
 * Servlet implementation class ItemDataCreateMaster
 */
@WebServlet("/ItemDataCreateMaster")
@MultipartConfig(location="C:\\Users\\hirok\\OneDrive\\ドキュメント\\mywebsite\\project\\WebContent\\img", maxFileSize=1048576)

public class ItemDataCreateMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDataCreateMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");



        try {
        	String itemName =  request.getParameter("item-name");
    		String itemPrice = request.getParameter("item-price");
    		String itemDetail = request.getParameter("item-detail");
    		String itemCategory = request.getParameter("item-category");


    		Part part = request.getPart("file");
            //part(主にjsp)から送られてきたファイル名を取得
            String name = this.getFileName(part);

			ItemDAO.createItem(itemName, itemDetail, itemPrice, name, itemCategory);

			 part.write(name);
			response.sendRedirect("ItemDataListMaster");

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}




    }
     private String getFileName(Part part){
          String name = null;
          for(String dispotion : part.getHeader("Content-Disposition").split(";")){
              if(dispotion.trim().startsWith("filename")){
                  name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"","").trim();
                  name = name.substring(name.lastIndexOf("\\") + 1 );
                  break;
              }
          }
          return name;
      }




}
