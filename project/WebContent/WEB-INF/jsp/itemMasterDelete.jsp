<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<h1>商品マスタ情報削除</h1>
		<div class="row">
			<a href="ItemDataListMaster" class="btn btn-primary">キャンセル</a>
			<div class="confirm">
				<form action="ItemDataDeleteMaster" method="post">
					<input type="hidden" name="id" value="${item.id}">
          			<input type="submit" class="btn btn-success" value="OK">
          		</form>
		     </div>
		  </div>
     </div>
</body>
</html>