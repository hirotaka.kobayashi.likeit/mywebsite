<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<div class="container">
		<h1>商品マスタ一覧</h1>

			<div class="text-right">
	        		<a href="ItemDataCreateMaster">新規登録</a>
	      	</div>

			<div class="section"></div>


			<div class="table-responsive">
             <table class="table table-striped">
	             <thead>
	               <tr>
	                 <th>商品名</th>
	                 <th></th>
	               </tr>
	             </thead>
	             <tbody>
	             	 <c:forEach var="item" items="${itemList}">
		                 <tr>
		                   <td>${item.name}</td>
		                   <td>
		                 		<a class="btn btn-primary" href="ItemDataDetail?id=${item.id}">詳細</a>
		                 		<a class="btn btn-success" href="ItemDataUpdateMaster?id=${item.id}">更新</a>
		             			<a class="btn btn-danger" href ="ItemDataDeleteMaster?id=${item.id}">削除</a>
		                   </td>
		                 </tr>
		              </c:forEach>
	             </tbody>
             </table>
           </div>
           <a href="Master">戻る</a>
	</div>
</body>
</html>