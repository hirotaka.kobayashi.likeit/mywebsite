<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<h1>ユーザー一覧</h1>
		<div class="section"></div>
		<div class="userSearch">
			<form action="UserDataMaster" method="post" class="form-horizontal">
			       <div class="form-group row">
					    <label for="login" class="col-sm-4 col-form-label">ログインID</label>
					    <div class="col-md-8">
					      <input type="text"  name="loginId">
					    </div>
				 	</div>
				  <div class="form-group row">
				    <label for="name" class="col-sm-4 col-form-label">ユーザー名</label>
				    <div class="col-md-8">
				      <input type="text"  name="user_name">
				    </div>
				  </div>
				 <div class="form-group row">
				    <label for="continent" class="col-form-label col-sm-2">生年月日</label>

				      <div class="col-sm-2">
				        <input type="date" name="date-start" id="date-start" class="form-control" />
				      </div>
				      <div class="col-xs-1 text-center">
				        ~
				      </div>
				      <div class="col-sm-2">
				        <input type="date" name="date-end" id="date-end" class="form-control"/>
				      </div>
			     </div>
				 <div class="text-right">
			     	<button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
			     </div>
		    </form>
	    </div>
	    <div class="section"></div>

	    <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>誕生日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
               		 <c:forEach var="user" items="${userList}" >
	                 	<tr>
	                 		<td>${user.loginId}</td>
	                 		<td>${user.name}</td>
	                 		<td>${user.birthDate}</td>
		                     <td>
		                  		<a class="btn btn-primary" href="UserDataDetailMaster?id=${user.id}">詳細</a>
		                  		<a class="btn btn-danger" href="UserDataDeleteMaster?id=${user.id}">削除</a>
		                     </td>
	                  	 </tr>
	                  </c:forEach>
               </tbody>
             </table>
         </div>
         <a href="Master">戻る</a>
     </div>

</body>
</html>