<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h3 class="title text-center">購入が完了しました</h3>


		<div class="row">
			<div class="col-7">
				<div class="text-center">
					<form action="Index">
						<button type="submit" class="bth btn-success">引き続き買い物をする</button>
					</form>
				</div>
			</div>
			<div class="col-5">
				<form action="UserData">
					<button type="submit" class="bth btn-success">ユーザー情報へ</button>
				</form>
		   </div>
		 </div>


		<h3 class="text-center detail">購入詳細</h3>

		<div class="section"></div>

		<div class="buyResult">
			<table class="table">
					<tr>
				      <th>購入日時</th>
				      <th>配達方法</th>
					  <th>合計金額</th>
				    </tr>
				    <tr>
				      <td class="center">${resultBDB.formatDate}</td>
					  <td class="center">${resultBDB.deliveryMethodName}</td>
					  <td class="center">${resultBDB.formatTotalPrice}円</td>
				    </tr>
			</table>
	    </div>

			<div class="section"></div>

		<div class="buyResult">
			<table class="table">
				<tr>
					<th class="center">商品名</th>
					<th class="center">単価</th>
				</tr>
				<c:forEach var="buyIDB" items="${buyIDBList}" >
					<tr>
						<td class="center">${buyIDB.name}</td>
						<td class="center">${buyIDB.formatPrice}円</td>
					</tr>
				</c:forEach>
				<tr>
			    	<td class="center">${resultBDB.deliveryMethodName}</td>
					<td class="center">${resultBDB.deliveryMethodPrice}円</td>
			    </tr>
			</table>
		</div>
		</div>
</body>
</html>