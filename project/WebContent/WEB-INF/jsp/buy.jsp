<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h5 class="title text-center">カートアイテム</h5>
		<div class="buy">
			<form action="BuyConfirm">
				<table class="table">
					<tr>
				      <th>商品名</th>
				      <th>単価</th>
					  <th>小計</th>
				    </tr>
				    <c:forEach var="cartInItem" items="${cart}" >
				    <tr>
					      <td>${cartInItem.name}</td>
					      <td>${cartInItem.price}円</td>
					      <td>${cartInItem.price}円</td>
				    </tr>
				    </c:forEach>
				    <tr>
				      <td></td>
				      <td> </td>
				      <td>
				      	<label>配達方法</label>
				      	<select name="delivery_method_id">
				      		<c:forEach var="dmdb" items="${dmdbList}" >
					      		<option value="${dmdb.id}">${dmdb.name}</option>
				      		</c:forEach>
				      	</select>　
				      </td>
				    </tr>
				</table>
				<div class="text-center">
						<button class="btn btn-success" type="submit" name="action">購入確認</button>
				</div>
			</form>
		 </div>
	</div>
</body>
</html>