<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/baselayout/header.jsp" />




	<div class="container">
		<p>${cartActionMessage}</p>
		<h5 class=" col s12 light">買い物かご</h5>
		<form action="ItemDelete" method="POST">
			<div class="row">
				<div class="col-6">
					<button type="submit" class="bth btn-danger">削除</button>
				</div>
				<div class="col-6">
					<a href="Buy"><button type="button" class="bth btn-success">レジに進む</button></a>
				</div>
			</div>
			<div class="section"></div>
			<div class="row">
				<c:forEach var="item" items="${cart}" varStatus="status">
				<div class="col-6">
					<div class="card" style="width: 20rem;">
							  <img src="img/${item.fileName}">
							  <div class="card-body">
								<input type=checkbox id="${status.index}" name="delete_item_id_list" value="${item.id}"><label for="${status.index}">削除</label>
							  </div>
					</div>
					<div class="section"></div>
				</div>
				</c:forEach>
			</div>
		</form>
	</div>

</body>
</html>