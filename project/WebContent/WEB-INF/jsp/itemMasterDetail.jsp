<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<div class="container">
		<h1>商品マスタ情報参照</h1>
		<div class="section"></div>

		<div class="row">
			 <div class="col-6">
			        <div class="card" style="width: 20rem;">
					  <img src="img/${item.fileName}">
					  <div class="card-body">
					  </div>
				 </div>
			 </div>
			 <div class="col-6">
			 	<h4>${item.name}</h4>
				<h5>${item.price}円</h5>
				<p>${item.detail}</p>
			 </div>
		</div>

		<div class="section"></div>
		<div class="section"></div>


		<h2>レビュー</h2>
		 <c:forEach var="rdb" items="${rdbList}">
			 <div class="review">
						<span class="material-icons">account_circle</span>${rdb.name}
						<br>
						${rdb.score} ${rdb.title}
						<br>
						${rdb.comment}
			 </div>
			  <div class="section"></div>
		 </c:forEach>
		<a href="ItemDataListMaster">戻る</a>
	</div>
</body>
</html>