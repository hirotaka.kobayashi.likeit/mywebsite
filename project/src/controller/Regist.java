package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String user_name = request.getParameter("user_name");
		String user_address = request.getParameter("user_address");
		String birth = request.getParameter("birth");
		String password = request.getParameter("password");
		String pwd_conf = request.getParameter("pwd_conf");

		if (loginId.equals("") ||  user_name.equals("") || user_address.equals("") || birth.equals("") || password.equals("") || pwd_conf.equals("") ) {
			request.setAttribute("errMsg", "入力された内容は正しくはありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
			return;
		}


		if (password.equals(pwd_conf)) {

			
			UserDataBeans userCheck = UserDAO.check(loginId);

			if (!(userCheck == null)) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくはありません");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regis.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDAO.regist(loginId, user_name, user_address, birth, password);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registResult.jsp");
			dispatcher.forward(request, response);


		} else {

			request.setAttribute("errMsg", "入力された内容は正しくはありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

}
