<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<h1>ユーザー情報参照</h1>
		<div class="section"></div>
		<table class="table">
			<tr>
				<td>ログインID</td>
				<td>${user.loginId}</td>
			</tr>
			<tr>
				<td>ユーザー名</td>
				<td>${user.name}</td>
			</tr>
			<tr>
				<td>住所</td>
				<td>${user.address}</td>
			<tr>
			<tr>
				<td>生年月日</td>
				<td>${user.birthDate}</td>
			<tr>
			<tr>
				<td><p>登録日時</p></td>
				<td>${user.createDate}</td>
			<tr>
		</table>
		<a href="UserDataMaster">戻る</a>
	</div>
</body>
</html>