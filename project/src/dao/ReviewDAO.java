package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReviewDataBeans;

public class ReviewDAO {

	public static void createReview(String score, String title, String comment, int itemId, int userId) throws SQLException {
		Connection con = null;

		try {
			con = DBManager.getConnection();
			String insertSQL = "INSERT INTO review (score, title, comment, item_id, user_id) VALUES (?, ?, ?, ?, ?) ";
			PreparedStatement pStmt = con.prepareStatement(insertSQL);
			pStmt.setString(1, score);
			pStmt.setString(2, title);
			pStmt.setString(3, comment);
			pStmt.setInt(4, itemId);
			pStmt.setInt(5, userId);

			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}



	}

	public static List<ReviewDataBeans>  findAllById(int itemId) throws SQLException {

		Connection con = null;

		try {

			con = DBManager.getConnection();
			String sql = "SELECT * FROM review JOIN user ON review.user_id = user.id WHERE item_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, itemId);
			ResultSet rs = pStmt.executeQuery();

			ArrayList<ReviewDataBeans>  reviewDataBeansList = new ArrayList<ReviewDataBeans>();

			while (rs.next()) {
				ReviewDataBeans rdb = new ReviewDataBeans();
				rdb.setName(rs.getString("name"));
				rdb.setScore(rs.getString("score"));
				rdb.setTitle(rs.getString("title"));
				rdb.setComment(rs.getString("comment"));
				reviewDataBeansList.add(rdb);

			}

			return reviewDataBeansList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}


	}

}
