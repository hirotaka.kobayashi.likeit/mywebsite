<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h2>購入詳細</h2>
		<div class="section"></div>
		<div class="buyDetail">
			<table class="table">
				<thead>
					<tr>
						<th>購入日時</th>
						<th>配達方法</th>
						<th>合計金額</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="center">${buyData.formatDate}</td>
						<td class="center">${buyData.deliveryMethodName}</td>
						<td class="center">${buyData.formatTotalPrice}円</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="section"></div>
		<div class="buyItemDetail">
			<table class="table">
				<thead>
					<tr>
						<th>商品名</th>
						<th>単価</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="buyIDB" items="${buyIDBList}" >
							<tr>
								<td class="center">${buyIDB.name}</td>
								<td class="center">${buyIDB.formatPrice}円</td>
							</tr>
					</c:forEach>
					<tr>
						<td class="center">${buyData.deliveryMethodName}</td>
						<td class="center">${buyData.deliveryMethodPrice}円</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>