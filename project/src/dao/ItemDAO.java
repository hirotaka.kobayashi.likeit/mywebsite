package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {


	public static ArrayList<ItemDataBeans> getAllItemDataBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item");
			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategoryId(rs.getInt("category_id"));
				itemList.add(idb);
			}

			return itemList;

		}  catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<ItemDataBeans> findItemCategoryId(int categoryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item where category_id = ?");
			st.setInt(1, categoryId);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategoryId(rs.getInt("category_id"));
				itemList.add(idb);
			}

			return itemList;

		}  catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}


	}

	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryId(rs.getInt("category_id"));
			}

			return item;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				st = con.prepareStatement("SELECT * FROM item ORDER BY id ASC");
			} else {
				st = con.prepareStatement("SELECT * FROM item WHERE name LIKE ? ORDER BY id ASC");
				st.setString(1,"%" + searchWord + "%");
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}

			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void createItem(String itemName,  String itemDetail , String itemPrice, String name, String itemCategory) throws SQLException{
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String insertSQL = "INSERT INTO item(name, detail, price, file_name, category_id) VALUES(?,?,?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			pStmt.setString(1, itemName);
			pStmt.setString(2, itemDetail);
			pStmt.setString(3, itemPrice);
			pStmt.setString(4, name);
			pStmt.setString(5, itemCategory);

			pStmt.executeUpdate();
		}  catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void updateItem(String itemName,  String itemDetail , String itemPrice, String name, String id) throws SQLException {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String updateSQL = "UPDATE item set name = ? , detail = ?, price = ?, file_name = ? WHERE id=? ";
			PreparedStatement pStmt = conn.prepareStatement(updateSQL);
			pStmt.setString(1, itemName);
			pStmt.setString(2, itemDetail);
			pStmt.setString(3, itemPrice);
			pStmt.setString(4, name);
			pStmt.setString(5, id);

			pStmt.executeUpdate();
		}  catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void deleteItem(String id) throws SQLException {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String deleteSQL = "DELETE FROM item WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(deleteSQL);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
