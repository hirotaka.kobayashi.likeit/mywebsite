<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h5 class="title text-center">購入</h5>
		<div class="buyConfirm">
				<table class="table">
					<tr>
				      <th>商品名</th>
				      <th>単価</th>
					  <th>小計</th>
				    </tr>
				    <c:forEach var="cartInItem" items="${cart}" >
					    <tr>
					    	<td class="center">${cartInItem.name}</td>
							<td class="center">${cartInItem.formatPrice}円</td>
							<td class="center">${cartInItem.formatPrice}円</td>
					    </tr>
				    </c:forEach>
				    <tr>
				    	<td class="center">${bdb.deliveryMethodName}</td>
						<td class="center">${bdb.deliveryMethodPrice}</td>
						<td class="center">${bdb.deliveryMethodPrice}円</td>
				    </tr>
				    <tr>
						<td class="center"></td>
						<td class="center">合計</td>
						<td class="center">${bdb.formatTotalPrice}円</td>
					</tr>
				</table>
				<div class="text-center">
					<form action="BuyResult" method="post">
						<button class="btn btn-success" type="submit">購入</button>
					</form>
				</div>
		</div>
	</div>
</body>
</html>