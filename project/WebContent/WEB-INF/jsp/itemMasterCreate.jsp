<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<h1 class="text-center">商品マスタ新規登録</h1>
		  <div class="newItemCreate">
		      <form action="ItemDataCreateMaster" method="post" enctype="multipart/form-data">
		      	<div class="form-group">
		      		<label for="item-name">商品名</label>
		      		<input type="text" class="form-control" name="item-name">
		      	</div>
		      	<div class="form-group">
		      		<label for="item-name">値段</label>
		      		<input type="number" class="form-control" name="item-price">
		      	</div>
		      	<div class="form-group">
		      		<label for="item-detail">詳細</label>
		      		<input type="text" class="form-control" name="item-detail">
		      	</div>
		      	<div class="form-group">
		      		<label>画像</label>
		      		<input type="file" class="form-control" name="file">
		      	</div>
		      	<div class="form-group">
		      		<label for="item-category">メーカー</label>
		      		<input type="number" class="form-control" name="item-category">
		      	</div>
		      	<div class="section"></div>
		      	<input type="submit" value="登録" class="btn btn-primary">
		      </form>
	      </div>
	      <a href="ItemDataListMaster">戻る</a>
	</div>
</body>
</html>