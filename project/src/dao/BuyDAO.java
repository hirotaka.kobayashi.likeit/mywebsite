package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDataBeans;

public class BuyDAO {

	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy(user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS
				 );
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getTotalPrice());
			st.setInt(3, bdb.getDelivertMethodId());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static BuyDataBeans getBuyDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT create_date , total_price, name, price FROM buy b "
							+ " JOIN delivery_method d "
							+ " ON b.delivery_method_id = d.id "
							+ " WHERE b.id = ? ");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			BuyDataBeans bdb = new BuyDataBeans();

			if (rs.next()) {

				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setDeliveryMethodName(rs.getString("name"));
			}


			return bdb;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ArrayList<BuyDataBeans> getAllBuyDataBeans(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM buy"
					+ " JOIN delivery_method"
					+ " ON buy.delivery_method_id = delivery_method.id"
					+ " WHERE buy.user_id = ?"
					+ " ORDER BY buy.id DESC ");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDataBeans> buyDataBeansList = new ArrayList<BuyDataBeans>();
			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("Id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodName(rs.getString("name"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));

				buyDataBeansList.add(bdb);

			}

			return buyDataBeansList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static int buyInfo(int userId ,int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int count = 0;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM buy"
					+ " JOIN buy_detail"
					+ " ON buy.id = buy_detail.buy_id"
					+ " WHERE user_id = ? AND item_id = ?");
			st.setInt(1, userId);
			st.setInt(2, itemId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				count++;
			}

			return count;


		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}



}
