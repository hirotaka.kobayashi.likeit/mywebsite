<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">

		<form action="ItemSearchResult"  class="form-box">
			<input class="form-input" type="text" name="search_word">
			<button class="form-button" type="submit">
				<span class="material-icons">youtube_searched_for</span>
			</button>
		</form>

		<div class="section"></div>

		<form action="ItemCategory">
			<label>メーカーで選ぶ</label>
			<select name="item_category">

				<option value="1">ヨネックス</option>
				<option value="2">バボラ</option>
				<option value="3">ヘッド</option>
				<option value="4">ウィルソン</option>

			</select>
			<button type="submit">実行</button>
		</form>

		<div class="section"></div>
		<a href="Index">商品一覧に戻る</a>

		<div class="section"></div>

	    <div class="container">
		    <div class="row">
				<c:forEach var="item" items="${itemList}">
					<div class="col-6">
						<div class="card" style="width: 30rem;">
							<img src="img/${item.fileName}">
							<div class="card-body">
								<p>${item.name}</p>
								<a href="Item?item_id=${item.id}" class="btn btn-primary">アイテム詳細へ</a>
							</div>
						</div>
						<div class="section"></div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>
