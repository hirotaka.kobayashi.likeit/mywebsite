<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<nav class="navbar navbar-expand-sm navbar-dark bg-primary mt-3 mb-3">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="Index">EC</a>
      <div class="collapse navbar-collapse justify-content-end">
          <ul class="navbar-nav">
          	 <% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

          	  <%if(isLogin){ %>
              <li class="nav-item ">
                  <a class="nav-link" href="UserData"><span class="material-icons">account_circle</span></a>
              </li>
              <%}else{ %>
              <li class="nav-item">
                  <a class="nav-link" href="Regist"><span class="material-icons">add</span></a>
              </li>
              <%} %>

              <li class="nav-item">
                  <a class="nav-link" href="Cart"><span class="material-icons">shopping_cart</span></a>
              </li>

              <%if(isLogin){ %>
               <li class="nav-item">
                  <a class="nav-link" href="Logout"><span class="material-icons">exit_to_app</span></a>
              </li>
              <%}else{ %>
               <li class="nav-item">
                  <a class="nav-link" href="Login"><span class="material-icons">vpn_key</span></a>
              </li>
              <%} %>
          </ul>
      </div>
</nav>